+++
title = "Community"
+++

This page attempts to explain how the Redox OS community is organised and help you navigate it.

We follow the [Rust Code Of Conduct](https://www.rust-lang.org/policies/code-of-conduct) for rules in all community/chat channels.

## [Announncements](https://matrix.to/#/#redox-announcements:matrix.org)

We do our announcements on this Matrix room, it's public and you don't need to login on Matrix to read it.

- #redox-announcements:matrix.org (Use this Matrix room address if you don't want to use the external Matrix link)

## [Chat](https://matrix.to/#/#redox-join:matrix.org)

Matrix is the official way to talk with Redox OS team and community (these rooms are English-only, we don't accept other languages because we don't understand them).

Matrix has several different clients. [Element](https://element.io/) is a commonly used choice, it works on web browsers, Linux, MacOSX, Windows, Android and iOS.

- Join [this](https://matrix.to/#/#redox-join:matrix.org) room and don't forget to request an invite to the Redox Matrix space.
- #redox-join:matrix.org (Use this Matrix room address if you don't want to use the external Matrix link)

(We recommend that you leave the "Join Requests" room after your entry on Redox space)

If you want to have a big discussion in our rooms, you should use a Element thread, it's more organized and easy to keep track if more discussions happen on the same room.

## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

A bit more formal way of communication with fellow Redox devs, but a little less quick and convenient like the chat. Submit an issue when you run into problems compiling, testing, or just would like to discuss a certain topic, be it features, code style, code inconsistencies, minor changes and fixes, etc.

If you want to create an account, read this [page](https://doc.redox-os.org/book/ch12-01-signing-in-to-gitlab.html).

If you have ready MRs (merge requests) you must send the links on the [MRs](https://matrix.to/#/#redox-mrs:matrix.org) room, before you join this room a request for a Matrix space invite must be sent on the [Join Requests](https://matrix.to/#/#redox-join:matrix.org) room.

That way your MR will not be forgotten or accumulate conflicts.

## [Lemmy](https://lemmy.world/c/redox)

Our alternative to Reddit, we post news and community threads.

## [Reddit](https://www.reddit.com/r/Redox/)

If you want a quick look at what's going on and talk about it.

[reddit.com/r/rust](https://www.reddit.com/r/rust) for related Rust news and discussions.

## [Fosstodon](https://fosstodon.org/@redox)

Our alternative to Twitter, we post news and community threads.

## [Twitter](https://twitter.com/redox_os)

News and community threads.

## [YouTube](https://www.youtube.com/@RedoxOS)

Demos and board meetings.

## [Forum](https://discourse.redox-os.org/)

This is our historical forum with old/classic questions, it's inactive and must be used for historical purposes, if you have a question, send on Matrix chat.

## [Talks](/talks/)

Redox talks given at various events and conferences.

## Note

Community outreach is an important part of Redox's success. If more people know about Redox, then more contributors are likely to step in, and everyone can benefit from their added expertise. You can make a difference by writing articles, talking to fellow OS enthusiasts, or looking for communities that would be interested in knowing more about Redox.
