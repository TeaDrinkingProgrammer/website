+++
title = "Donate/Merch"
+++

## Redox OS Nonprofit

Redox OS has a Colorado (US) incorporated 501(c)(4) nonprofit that manages
donations. Donations to the Redox OS nonprofit will be used as determined by
the Redox OS volunteer board of directors.

You can donate to Redox OS the following ways:

 - [Patreon](https://www.patreon.com/redox_os)
 - [Donorbox](https://donorbox.org/redox-os)
 - For more donation options, please contact donate@redox-os.org

## Merch

We sell T-shirts on Teespring, you can buy them [here](https://redox-os.creator-spring.com/).

Each sale is a donation to the Redox OS Nonprofit.

## Jeremy Soller

Jeremy Soller, is the creator, maintainer, and lead developer of Redox OS.
Donations to Jeremy Soller are treated as a taxable gift, and will be used at
his discretion.

You can donate to Jeremy Soller the following ways:

- [Liberapay](https://liberapay.com/redox_os)
- [Paypal](https://www.paypal.me/redoxos)
- Jeremy Soller no longer accepts Bitcoin or Ethereum donations. Do not send
  anything to the previously listed addresses, as it will not be received.

The following patrons have donated $4 or more to Jeremy Soller for use in developing Redox OS:
{{% partial "donors/jackpot51.html" %}}
